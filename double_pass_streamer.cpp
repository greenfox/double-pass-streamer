#include "double_pass_streamer.h"
#include "core/math/math_funcs.h"


void DoublePassStreamer::emerge_block(Ref<VoxelBuffer> out_buffer, Vector3i origin, int lod){
	ERR_FAIL_COND(out_buffer.is_null());
	if (pass_one(out_buffer,origin,lod) == true){
			real_t scale = pow(2,lod);
			Vector3i dataSize = (**out_buffer).get_size();
			Vector3 realPos = Vector3();
			for (int x = 0 ; x < dataSize.x; x++ ){
				realPos.x = (x*scale) + origin.x;
				for (int y = 0 ; y < dataSize.y; y++ ){
					realPos.y = (y*scale) + origin.y;
					for (int z = 0 ; z < dataSize.z; z++ ){
						realPos.z = (z*scale) + origin.z;
						real_t v = pass_two(realPos)/scale;
						(**out_buffer).set_voxel_f(v,x,y,z,_voxel_type);
					}
				}
			}
		}
		

}

bool DoublePassStreamer::pass_one(Ref<VoxelBuffer> out_buffer, Vector3i origin, int lod){
	ScriptInstance *script = get_script_instance();
	if (script) {
		// Call script to generate buffer
		Variant arg1 = out_buffer;
		Variant arg2 = origin.to_vec3();
		Variant arg3 = lod;
		const Variant *args[3] = { &arg1, &arg2, &arg3 };
		Variant::CallError err;
		bool output = script->call("pass_one", args, 3, err);
		return output;
	}
	return false;
}

real_t DoublePassStreamer::pass_two(Vector3 coord){
	ScriptInstance *script = get_script_instance();
	if (script) {
		// Call script to generate buffer
		Variant arg1 = coord;
		const Variant *args[1] = { &arg1 };
		Variant::CallError err;
		real_t output = script->call("pass_two", args, 1, err);
		return output;
	}
	return 1.0;
}

void DoublePassStreamer::set_voxel_type(int t) {
	_voxel_type = t;
}

int DoublePassStreamer::get_voxel_type() const {
	return _voxel_type;
}

// func emerge_block(out_buffer:VoxelBuffer, origin:Vector3, lod:int):
void DoublePassStreamer::_bind_methods(){
	// ClassDB::bind_method(D_METHOD("emerge_block", "out_buffer", "origin", "lod"), &DoublePassStreamer::emerge_block);
	// // // ClassDB::bind_method(D_METHOD("_pass_one", "out_buffer", "origin_in_voxels", "lod"), &DoublePassStreamer::pass_one);
	// // ClassDB::bind_method(D_METHOD("_pass_two", "coord"), &DoublePassStreamer::pass_two);

	ClassDB::bind_method(D_METHOD("set_voxel_type", "id"), &DoublePassStreamer::set_voxel_type);
	ClassDB::bind_method(D_METHOD("get_voxel_type"), &DoublePassStreamer::get_voxel_type);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "voxel_type", PROPERTY_HINT_RANGE, "0,255,1"), "set_voxel_type", "get_voxel_type");

}

DoublePassStreamer::DoublePassStreamer(){
	_voxel_type = 1;
}

