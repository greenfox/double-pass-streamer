/* double_pass_streamer.h */

#ifndef DOUBLE_PASS_STREAMER_H
#define DOUBLE_PASS_STREAMER_H

#include "modules/voxel/streams/voxel_stream.h"

class DoublePassStreamer : public VoxelStream {
    GDCLASS(DoublePassStreamer, VoxelStream);
private:
	int _voxel_type;
protected:
    static void _bind_methods();
public:
	virtual void emerge_block(Ref<VoxelBuffer> out_buffer, Vector3i origin, int lod);
	virtual bool pass_one(Ref<VoxelBuffer> out_buffer, Vector3i origin, int lod);
	virtual real_t pass_two(Vector3 coord);

	DoublePassStreamer();

	void set_voxel_type(int t);
	int get_voxel_type() const;	
};

// func emerge_block(out_buffer:VoxelBuffer, origin:Vector3, lod:int):

#endif // DOUBLE_PASS_STREAMER_H