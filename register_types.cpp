#include "register_types.h"

#include "double_pass_streamer.h"

void register_double_pass_streamer_types() {
	ClassDB::register_class<DoublePassStreamer>();
}

void unregister_double_pass_streamer_types() {
}
